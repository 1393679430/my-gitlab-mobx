import React from 'react'
import { inject, observer } from 'mobx-react';
import Child from './components/Child'

function App({ order }) {
  console.log(order, 'order');
  return (
    <div className="App">
      hello

      <div>order amount: {order.amount}</div>
      <button onClick={() => order.add('sss')}>add amount</button> 
     <Child />
    </div>
  );
}

export default inject('order')(observer(App));;
