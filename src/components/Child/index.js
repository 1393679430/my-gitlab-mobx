import React from 'react'; 
import { inject, observer } from 'mobx-react';

function Child({ order }) {
  
  const { amount, totalPrice } = order

  return (
    <div className="Child">
      <br/>
      <br/>
      this is child
      <div>
        <p>amount: {amount}</p>
        <p>totalPrice: {totalPrice}</p>
      </div>
    </div>
  );
}

export default inject('order')(observer(Child));