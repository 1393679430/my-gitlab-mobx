import { observable, decorate, action, computed } from 'mobx';

class Order {
  price = 2
  amount = 1
  
  add = (payload) => {
    console.log(payload, 'payload');
    this.amount += 1
  }

  get totalPrice() {
    return this.price * this.amount
  }
}
decorate(Order, {
  price: observable,
  amount: observable,
  add: action,
  totalPrice: computed
});
export default Order;